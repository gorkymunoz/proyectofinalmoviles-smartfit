package com.example.smartfit.Adapters

import android.net.Uri
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.smartfit.Holders.PostViewHolder
import com.example.smartfit.Models.Post
import com.example.smartfit.R
import com.google.firebase.firestore.DocumentChange
import com.google.firebase.firestore.FirebaseFirestore

class PostRecyclerViewAdapter:RecyclerView.Adapter<PostViewHolder>(){

    val mDatabase = FirebaseFirestore.getInstance()
    var posts: MutableList<Post> = mutableListOf()

    init {
        val postsRef = mDatabase.collection("Posts")

        postsRef.addSnapshotListener{
            snapshot,error ->
            if (error!=null){
                return@addSnapshotListener
            }
            for (doc in snapshot!!.documentChanges){
                when(doc.type){
                    DocumentChange.Type.ADDED ->{
                        val post = Post(
                            doc.document.getString("title"),
                            doc.document.getString("description"),
                            doc.document.getString("user"),
                            doc.document.getDate("date"),
                            doc.document.getString("imageUrl")
                        )
                        Log.d("post",post.toString())
                        posts.add(post)
                        notifyItemInserted(posts.size-1)
                    }
                    else ->return@addSnapshotListener
                }
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostViewHolder {

        val view =LayoutInflater.from(parent.context)
            .inflate(R.layout.card_post,parent,false)
        return PostViewHolder(view)

    }

    override fun getItemCount(): Int {
        return posts.size
    }

    override fun onBindViewHolder(holder: PostViewHolder, position: Int) {

        holder.setImage(posts[position].imageUrl!!)
        holder.titlePost.text =posts[position].title
        holder.autorPost.text = posts[position].user
        holder.datePost.text = posts[position].date.toString()
    }

}