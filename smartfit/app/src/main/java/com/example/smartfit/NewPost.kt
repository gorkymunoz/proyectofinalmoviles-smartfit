package com.example.smartfit


import android.app.Activity.RESULT_OK
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import kotlinx.android.synthetic.main.fragment_new_post.*
import android.graphics.BitmapFactory
import android.graphics.Bitmap
import android.R.attr.data
import android.net.Uri
import android.text.TextUtils
import android.util.Log
import android.widget.ProgressBar
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import com.example.smartfit.Models.Post
import com.google.android.gms.tasks.Continuation
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.UploadTask
import kotlinx.android.synthetic.main.fragment_register.*
import java.io.FileNotFoundException
import java.util.*


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class NewPost : Fragment() {

    lateinit var mDatabase:FirebaseFirestore
    lateinit var mStore:FirebaseStorage
    lateinit var mAuth:FirebaseAuth
    lateinit var currentUser:FirebaseUser
    lateinit var pbNewPost : ProgressBar
    private var imageUri: Uri? = null
    private var uriStorage: Uri? = null
    lateinit var userName:String
    private lateinit var clContentNewPost: ConstraintLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mAuth = FirebaseAuth.getInstance()
        currentUser = mAuth.currentUser!!
        mDatabase = FirebaseFirestore.getInstance()
        mStore = FirebaseStorage.getInstance()

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view:View = inflater.inflate(R.layout.fragment_new_post, container, false)
        pbNewPost = view.findViewById(R.id.pbNewPost)
        clContentNewPost = view.findViewById(R.id.clContentNewPost)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        pbNewPost.visibility = View.GONE
        getUsername()
        imageButton.setOnClickListener {
            val intent = Intent(Intent.ACTION_GET_CONTENT)
            intent.type = "image/*"
            startActivityForResult(intent, TAG)
        }
        btnPublishNewPost.setOnClickListener {
            if (validateForm()){
                publishNewPost()
            }
        }
    }

    private fun publishNewPost(){

        pbNewPost.visibility = View.VISIBLE
        clContentNewPost.visibility = View.GONE

        var newPost = Post(etTitleNewPost.text.toString(),
            etDescriptionNewPost.text.toString(),
            userName,
            Date(),"")
        publishImage(newPost)
    }



    private fun publishImage(data:Post){

        if(imageUri!=null){

            val filepath = mStore.reference.child("PostsImages")
            val imgReference = filepath.child(imageUri?.lastPathSegment!!)
            val uploadTask:UploadTask = imgReference.putFile(imageUri!!)
            val urlTask = uploadTask.continueWithTask(Continuation<UploadTask.TaskSnapshot, Task<Uri>> { task ->
                if (!task.isSuccessful) {
                    task.exception?.let {
                        throw it
                    }
                }
                return@Continuation imgReference.downloadUrl
            }).addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    uriStorage = task.result
                    Toast.makeText(context,uriStorage.toString(),Toast.LENGTH_LONG).show()
                } else {
                    Log.e("nourl","no hay url")
                }
            }
            urlTask.addOnSuccessListener {
                data.imageUrl = uriStorage.toString()
                savePostDatabase(data)
            }

        }else{
            savePostDatabase(data)
        }



    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK && requestCode == TAG) {
            try {
                imageUri = data!!.data
                val imageStream = context!!.contentResolver.openInputStream(imageUri!!)
                val selectedImage = BitmapFactory.decodeStream(imageStream)

                imageButton.setImageBitmap(selectedImage)

            } catch (e: FileNotFoundException) {
                e.printStackTrace()
            }
        }
    }

    private fun getUsername() {
        val user = mDatabase.collection("Users").document(currentUser.uid).get()
        user.addOnCompleteListener { task ->
            if (task.isSuccessful) {
                val userDoc = task.result
                if(userDoc!!.exists()){
                    userName = userDoc.get("Username").toString()
                }else{
                    Toast.makeText(context,"No existe el usuario actual",Toast.LENGTH_SHORT).show()
                }
            } else {
                Toast.makeText(context,task.exception?.message,Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun savePostDatabase(newPost: Post){
        Log.e("db",newPost.toString())
        mDatabase.collection("Posts")
            .add(newPost)
            .addOnCompleteListener{ taskSave ->
                if(taskSave.isSuccessful){
                    Toast.makeText(context,R.string.postUploaded,
                        Toast.LENGTH_SHORT).show()
                    updateUI()
                }else{
                    Toast.makeText(context, taskSave.exception?.message.toString(),
                        Toast.LENGTH_SHORT).show()
                }
            }
    }

    private fun updateUI(){
        clContentNewPost.visibility = View.VISIBLE
        etTitleNewPost.text?.clear()
        etDescriptionNewPost.text?.clear()
        imageButton.setImageResource(R.drawable.ic_forum)
        pbNewPost.visibility = View.GONE



    }

    private fun validateForm():Boolean{
        var valid = true
        val titleNewPost = etTitleNewPost.text.toString()
        val descriptionNewPost = etDescriptionNewPost.text.toString()
        if(TextUtils.isEmpty(titleNewPost)){
            etTitleNewPost.error = getText(R.string.required)
            valid = false
        }else{
            etTitleNewPost.error = null
        }

        if(TextUtils.isEmpty(descriptionNewPost)){
            etDescriptionNewPost.error = getText(R.string.required)
            valid = false
        }else{
            etTitleNewPost.error = null
        }
        return valid
    }


    companion object{
        private const val TAG = 1
    }
}