package com.example.smartfit


import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.widget.Toolbar
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.NavigationUI
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_main.*


class MainNavigationActivity: AppCompatActivity() {

    private lateinit var auth: FirebaseAuth
    private lateinit var fabLogOut: FloatingActionButton

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        fabLogOut = findViewById(R.id.fabLogOut)
        auth = FirebaseAuth.getInstance()
        if(auth.currentUser!=null){
            toolbarMain.inflateMenu(R.menu.toolbar_main)
            fabLogOut.setOnClickListener {
                auth.signOut()
                val intent = Intent(this,MainActivity::class.java)
                startActivity(intent)
                this.finish()
            }
        }else{
            fabLogOut.visibility = View.GONE
            toolbarMain.inflateMenu(R.menu.toolbar_not_logged)
        }

           // .setupWithNavController(navController, appBarConfiguration)

        val navController = findNavController(R.id.navHostFragmentMain)
       val appBarConfiguration = AppBarConfiguration(navController.graph)
        findViewById<androidx.appcompat.widget.Toolbar>(R.id.toolbarMain)
            .setupWithNavController(navController,appBarConfiguration)
        findViewById<BottomNavigationView>(R.id.bnvNavegacionPrincinpal)
            .setupWithNavController(navController)

    }

    /*override fun onCreateOptionsMenu(menu: Menu): Boolean {
        MenuInflater(this).inflate(R.menu.toolbar_main, menu)
        return super.onCreateOptionsMenu(menu)
    }*/

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        NavigationUI.onNavDestinationSelected(item!!, Navigation.findNavController(this, R.id.navHostFragmentMain))
        return super.onOptionsItemSelected(item)
    }

}