package com.example.smartfit


import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.navigation.findNavController
import com.google.firebase.auth.FirebaseAuth


class Sign_In : Fragment() {


    lateinit var etEmail: EditText
    lateinit var etPassword: EditText

    private lateinit var auth: FirebaseAuth
    lateinit var btnSignInFragment: Button

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var view:View = inflater.inflate(R.layout.fragment_sign_in, container, false)
        btnSignInFragment = view.findViewById(R.id.btnSignInFragment)
        etEmail = view.findViewById(R.id.etEmailSignIn)
        etPassword = view.findViewById(R.id.etPasswordSignIn)
        // Inflate the layout for this fragment
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btnSignInFragment.setOnClickListener {
            if(validateForm()){
                signIn(etEmail.text.toString(),etPassword.text.toString())
            }
        }
    }

    private fun signIn(email: String, password: String) {
        activity?.let {
            auth.signInWithEmailAndPassword(email,password)
                .addOnCompleteListener(it){
                task -> if(task.isSuccessful){
                    Toast.makeText(context,"Credenciales correctas",Toast.LENGTH_SHORT).show()
                    //view?.findNavController()?.navigate(R.id.,null)

                    val intent = Intent(context, MainNavigationActivity::class.java)
                    startActivity(intent)
                    activity?.finish()
                }else{
                    Toast.makeText(context, task.exception.toString(), Toast.LENGTH_SHORT ).show()
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        auth = FirebaseAuth.getInstance()
    }


    private fun validateForm():Boolean{
        var valid = true

        val email = etEmail.text.toString()
        val password = etPassword.text.toString()

        if(TextUtils.isEmpty(email)){
            etEmail.error = getText(R.string.required)
            valid = false
        }else{
            etEmail.error = null
        }

        if(TextUtils.isEmpty(password)){
            etPassword.error = getText(R.string.required)
            valid = false
        }else{
            etPassword.error = null
        }
        return valid
    }


}
