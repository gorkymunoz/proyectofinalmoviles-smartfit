package com.example.smartfit


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.smartfit.Adapters.PostRecyclerViewAdapter
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.fragment_forum.*

// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class MainNavigation : Fragment() {

    private lateinit var rvPosts: RecyclerView
    private lateinit var mAuth: FirebaseAuth
    private lateinit var btnNewPost: Button

    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
        mAuth = FirebaseAuth.getInstance()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view:View = inflater.inflate(R.layout.fragment_forum, container, false)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btnNewPost = view.findViewById(R.id.btnNewPost)
        if(mAuth.currentUser==null){

            btnNewPost.visibility = View.GONE
        }else{
            btnNewPost.setOnClickListener {
                view.findNavController().navigate(R.id.action_navForum_to_newPost)
            }
        }


        rvPosts = view.findViewById(R.id.rvPosts)
        rvPosts.layoutManager = LinearLayoutManager(this.context)
        rvPosts.adapter =PostRecyclerViewAdapter()
    }

    companion object {
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            MainNavigation().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}
