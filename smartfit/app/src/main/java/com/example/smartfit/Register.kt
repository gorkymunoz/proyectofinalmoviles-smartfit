package com.example.smartfit


import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.facebook.*
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.facebook.login.widget.LoginButton
import com.google.firebase.auth.*
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.fragment_register.*
import android.content.pm.PackageManager
import android.content.pm.PackageInfo
import android.util.Base64
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException


class Register : Fragment() {

    private lateinit var mAuth:FirebaseAuth
    private lateinit var db: FirebaseFirestore
    private lateinit var etEmail: EditText
    private lateinit var etPassword: EditText
    private lateinit var btnSignInFragment:Button
    private lateinit var loginButton: LoginButton
    private lateinit var callBackManager:CallbackManager

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view:View = inflater.inflate(R.layout.fragment_register, container, false)
        etEmail = view.findViewById(R.id.etEmail)

        etPassword = view.findViewById(R.id.etPassword)
        btnSignInFragment = view.findViewById(R.id.btnSignInFragment)
        loginButton = view.findViewById(R.id.btnFacebook)

        return view
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mAuth = FirebaseAuth.getInstance()
        db = FirebaseFirestore.getInstance()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btnSignInFragment.setOnClickListener {
            if(validateForm()){
                createAccount(etEmail.text.toString().trim(),etPassword.text.toString())
            }
        }
    }

    override fun onStart() {
        super.onStart()
        val currentUser= mAuth.currentUser
        if(currentUser==null){

            try {
                val info =  context!!.packageManager.getPackageInfo(
                    "com.example.smartfit",
                    PackageManager.GET_SIGNATURES
                )
                for (signature in info.signatures) {
                    val md = MessageDigest.getInstance("SHA")
                    md.update(signature.toByteArray())
                    Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT))
                }
            } catch (e: PackageManager.NameNotFoundException) {

                Log.e("error",e.toString())

            } catch (e: NoSuchAlgorithmException) {

                Log.e("error",e.toString())

            }




            Log.e("entro","currentuser null")
            callBackManager = CallbackManager.Factory.create()
            loginButton.fragment = this

            loginButton.registerCallback(callBackManager,object:FacebookCallback<LoginResult>{
                override fun onSuccess(result: LoginResult?) {
                    Log.d("success","si entro")
                    handleFacebookToken(result!!.accessToken)
                }

                override fun onCancel() {
                    Toast.makeText(context,"User cancelled it",Toast.LENGTH_SHORT).show()
                }
                override fun onError(error: FacebookException?) {
                    Log.e("errorfb",error.toString())
                    Toast.makeText(context,error.toString(),Toast.LENGTH_SHORT).show()
                }
            })
        }else{
            Log.e("else","currentuser!=null")
            Log.e("displayName",currentUser.email)
        }
    }

    fun handleFacebookToken(accessToken:AccessToken ){
        val authCredential: AuthCredential = FacebookAuthProvider.getCredential(accessToken.token)
        activity?.let {
            mAuth.signInWithCredential(authCredential)
                .addOnCompleteListener(it){
                        task -> if(task.isSuccessful)
                { val fbUser: FirebaseUser? = mAuth.currentUser
                    Toast.makeText(context,fbUser?.displayName.toString(),
                        Toast.LENGTH_SHORT).show()
            }else{
                    Toast.makeText(context, task.exception?.message.toString(),
                        Toast.LENGTH_SHORT).show()
                }
            }
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        callBackManager.onActivityResult(requestCode,resultCode,data)

    }

    private fun createAccount(email:String,password:String){

        activity?.let {
            mAuth.createUserWithEmailAndPassword(email,password).addOnCompleteListener(it){
                task ->
                if(task.isSuccessful){
                    val userId = mAuth.currentUser!!.uid

                    val data = HashMap<String,Any>()
                    data["Username"] = etUsername.text.toString().trim()
                    data["Email"] = etEmail.text.toString().trim()

                    db.collection("Users")
                        .document(userId)
                        .set(data)
                        .addOnCompleteListener{ taskSave ->
                            if(taskSave.isSuccessful){
                                Toast.makeText(context,R.string.userRegistered,
                                    Toast.LENGTH_SHORT).show()
                        }else{
                                Toast.makeText(context, taskSave.exception?.message.toString(),
                                    Toast.LENGTH_SHORT).show()
                            }}
                }else{
                    Toast.makeText(context, task.exception?.message.toString(),
                        Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    private fun validateForm():Boolean{
        var valid = true

        val username:String = etUsername.text.toString()
        val email = etEmail.text.toString()
        val password = etPassword.text.toString()

        if(TextUtils.isEmpty(username)){
            etUsername.error = getText(R.string.required)
            valid = false
        }else{
            etUsername.error = null
        }

        if(TextUtils.isEmpty(email)){
            etEmail.error = getText(R.string.required)
            valid = false
        }else{
            etEmail.error = null
        }

        if(TextUtils.isEmpty(password)){
            etPassword.error = getText(R.string.required)
            valid = false
        }else{
            etPassword.error = null
        }
        return valid
    }

}
