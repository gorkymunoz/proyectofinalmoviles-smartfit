package com.example.smartfit


import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.navigation.findNavController
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.fragment_first.*


class First : Fragment() {

    private lateinit var mAuth:FirebaseAuth
    internal lateinit var btnSignUp: Button
    internal lateinit var btnSignIn: Button


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view:View =inflater.inflate(R.layout.fragment_first, container, false)
        btnSignIn=view.findViewById(R.id.btnSignIn)
        btnSignUp=view.findViewById(R.id.btnSignUp)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btnSignUp.setOnClickListener {
            goToSignUp()
        }
        btnSignIn.setOnClickListener {
            goToSignIn()
        }
        btnFreeTrial.setOnClickListener {
            goToMainNavigation()
        }
    }

    fun goToSignUp(){
        view?.findNavController()?.navigate(R.id.action_first_to_register)
    }

    fun goToSignIn(){
        view?.findNavController()?.navigate(R.id.action_first_to_signIn)
    }
    fun goToMainNavigation(){
        //view?.findNavController()?.navigate(R.id.action_first_to_main)
        val intent = Intent(context, MainNavigationActivity::class.java)
        startActivity(intent)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mAuth = FirebaseAuth.getInstance()
    }

    override fun onStart() {
        super.onStart()
        val currentUser= mAuth.currentUser
        if(currentUser!=null){
            val intent = Intent(context,MainNavigationActivity::class.java)
            startActivity(intent)
            activity?.finish()
        }
    }


}
