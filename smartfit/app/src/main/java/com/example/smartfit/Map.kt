package com.example.smartfit


import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityCompat
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions

private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class Map : Fragment(), OnMapReadyCallback {

    lateinit var  mGoogleMap: GoogleMap
    lateinit var mMapView: MapView


    override fun onMapReady(googleMap: GoogleMap) {


        MapsInitializer.initialize(context)
        mGoogleMap = googleMap

        setUpMap()
        mGoogleMap.mapType = GoogleMap.MAP_TYPE_SATELLITE

        val smartFit = LatLng(-0.1738262,-78.4845346)
        mGoogleMap.addMarker(MarkerOptions().position(smartFit).title("Smarfit Plataforma Gubernamental"))
        mGoogleMap.animateCamera((CameraUpdateFactory.newLatLngZoom(smartFit,17f)))

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view:View =inflater.inflate(R.layout.fragment_map, container, false)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mMapView = view.findViewById(R.id.mapView)
        if(mMapView!=null){
            mMapView.onCreate(null)
            mMapView.onResume()
            mMapView.getMapAsync(this)

        }
    }

    private fun setUpMap() {
        if (ActivityCompat.checkSelfPermission(context!!,
                android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            this.activity?.let {
                ActivityCompat.requestPermissions(
                    it,
                    arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION), 177)
            }
            return
        }else{
            mGoogleMap.isMyLocationEnabled = true
        }
    }


}
