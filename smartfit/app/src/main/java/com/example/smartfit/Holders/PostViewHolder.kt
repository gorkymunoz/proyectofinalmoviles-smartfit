package com.example.smartfit.Holders

import android.content.Context
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.smartfit.R

class PostViewHolder(itemView:View): RecyclerView.ViewHolder(itemView){

    fun setImage(url:String){
        val imagePost =itemView.findViewById<ImageView>(R.id.imgPost)
        Glide
            .with(itemView.context)
            .load(url)
            .into(imagePost)
    }

    var titlePost = itemView.findViewById<TextView>(R.id.tvTitlePost)
    var autorPost = itemView.findViewById<TextView>(R.id.tvAutorPost)
    var datePost = itemView.findViewById<TextView>(R.id.tvDatePost)

}