package com.example.smartfit.Models

import android.net.Uri
import java.util.*

data class Post(
    var title:String?,
    var description:String?,
    var user:String?,
    var date:Date?,
    var imageUrl: String?)

